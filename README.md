# Text Adventure

A simple text adventure game for the Online_A course.
Project Tiny is a rather volatile, experimental package by Unity for small and fast WebGL games.

It's quite likely that by the time you're reading this, the package already fundamentally changed several times.
